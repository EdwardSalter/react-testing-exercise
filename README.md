# Simple React Calculator Testing Workshop

This repo holds the companion application for a workshop on UI testing.

Run the following to start the app:

```bash
git clone https://gitlab.com/EdwardSalter/react-testing-exercise.git
cd react-testing-exercise
npm install
npm start
```

## Required Tools and Documentation

### WallabyJS

VSCode: https://marketplace.visualstudio.com/items?itemName=WallabyJs.wallaby-vscode
WebStorm: https://plugins.jetbrains.com/plugin/15742-wallaby

### React Testing Library

https://testing-library.com/docs/react-testing-library/intro/

### Jest

https://jestjs.io

### jest-dom

https://github.com/testing-library/jest-dom

## Workshop Agenda

1. Get individual environments setup
   1. Clone and start repo
   2. Add Wallaby plugin to editors and register
2. Walkthrough of demo application
3. Introduce Wallaby
   1. What it does?
   2. How to start it?
   3. Companion app
4. Discuss different types of testing
5. Brief description of TDD
6. Test utility functions exercise
   1. Write tests together, take in turns to control screen.
   2. Be sure to discuss common pitfalls
7. Discuss coverage
8. Talk about component integration testing
9. Exercise testing components
