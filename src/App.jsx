import "./App.css";
import MedianCalculator from "./components/MedianCalculator";
import DaysCalculator from "./components/DaysCalculator";

function App() {
  return (
    <div className="App">
      <h1>Calculators</h1>

      <MedianCalculator />
      <DaysCalculator />
    </div>
  );
}

export default App;
