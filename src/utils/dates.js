const DAY_IN_MS = 1000 * 60 * 60 * 24;

export function differenceInDays(date1, date2) {
  if (!date1 || !date2) return 0;

  const d1 = new Date(date1).valueOf();
  const d2 = new Date(date2).valueOf();

  return Math.ceil((d2 - d1) / DAY_IN_MS);
}
