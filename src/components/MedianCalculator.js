import React, { useState } from "react";
import { LabelledInput } from "./LabelledInput";
import clsx from "clsx";
import { calculateMedian } from "../utils/maths";

const MedianCalculator = () => {
  const [{ input1, input2 }, setValues] = useState({
    input1: "",
    input2: "",
  });
  const value = calculateMedian(input1, input2);

  function onMedianChange(e) {
    setValues((prevValue) => {
      return {
        ...prevValue,
        [e.target.name]: e.target.value,
      };
    });
  }

  return (
    <>
      <h2>Median Calculator</h2>
      <p>Enter 2 numbers to get the median</p>
      <div className="input-container">
        <LabelledInput
          name="input1"
          value={input1}
          onChange={onMedianChange}
          label="First Value"
          error={isNaN(Number(input1))}
        />
        <LabelledInput
          name="input2"
          value={input2}
          onChange={onMedianChange}
          label="Second Value"
          error={isNaN(Number(input2))}
        />
      </div>

      <div>
        <label htmlFor="median">Median:</label>
        &nbsp;
        <output
          id="median"
          className={clsx({
            positive: value > 0,
            negative: value < 0,
          })}
        >
          {value.toString()}
        </output>
      </div>
    </>
  );
};

export default MedianCalculator;
