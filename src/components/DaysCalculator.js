import React, { useState } from "react";
import { LabelledInput } from "./LabelledInput";
import clsx from "clsx";
import { differenceInDays } from "../utils/dates";

const DaysCalculator = () => {
  const [{ dateInput1, dateInput2 }, setDateInputs] = useState({
    dateInput1: "",
    dateInput2: "",
  });

  function onDaysChange(e) {
    setDateInputs((prevState) => {
      return {
        ...prevState,
        [e.target.name]: e.target.value,
      };
    });
  }

  const daysDifference = differenceInDays(dateInput1, dateInput2);

  return (
    <>
      <h2>Days Different Calculator</h2>
      <div className="input-container">
        <LabelledInput
          type="date"
          name="dateInput1"
          value={dateInput1}
          onChange={onDaysChange}
          label="First Date"
        />
        <LabelledInput
          type="date"
          name="dateInput2"
          value={dateInput2}
          onChange={onDaysChange}
          label="Second Date"
        />
      </div>

      <div>
        <label htmlFor="days">Difference in days:</label>
        &nbsp;
        <output
          id="days"
          className={clsx({
            positive: daysDifference > 0,
            negative: daysDifference < 0,
          })}
        >
          {daysDifference.toString()}
        </output>
      </div>
    </>
  );
};

export default DaysCalculator;
