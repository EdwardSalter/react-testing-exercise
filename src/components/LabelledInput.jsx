import clsx from "clsx";

export const LabelledInput = ({ error, ...props }) => {
  return (
    <div className="input-column">
      <label htmlFor={props.name}>{props.label}</label>
      <input
        {...props}
        id={props.id || props.name}
        className={clsx([
          props.className,
          {
            error: !!error,
          },
        ])}
      />
    </div>
  );
};
